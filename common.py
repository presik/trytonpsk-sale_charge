# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool


def parcel_weight(parcel, uom_field='uom'):
    pool = Pool()
    Uom = pool.get('product.uom')
    ModelData = pool.get('ir.model.data')
    kilogram = Uom(ModelData.get_id('product', 'uom_kilogram'))

    weight = 0
    for line in parcel:
        product = getattr(line, 'product', None)
        quantity = getattr(line, 'quantity', None)
        uom = getattr(line, uom_field, None)
        if not all([product, quantity, uom]):
            continue

        if product.weight is not None:
            internal_quantity = Uom.compute_qty(
                uom, quantity, product.default_uom, round=False)
            weight += Uom.compute_qty(
                product.weight_uom, internal_quantity * product.weight,
                kilogram, round=False)
        elif uom.category == kilogram.category:
            weight += Uom.compute_qty(uom, quantity, kilogram, round=False)
    return weight


def parcel_volume(parcel, uom_field='uom'):
    pool = Pool()
    Uom = pool.get('product.uom')
    ModelData = pool.get('ir.model.data')
    cubic_meter = Uom(ModelData.get_id('product', 'uom_cubic_meter'))
    volume = 0
    for line in parcel:
        product = getattr(line, 'product', None)
        quantity = getattr(line, 'quantity', None)
        uom = getattr(line, uom_field, None)

        if not all([product, quantity, uom]):
            continue

        if product.volume is not None:
            internal_quantity = Uom.compute_qty(
                uom, quantity, product.default_uom, round=False)
            volume += Uom.compute_qty(
                product.volume_uom, internal_quantity * product.volume,
                cubic_meter, round=False)
        elif uom.category == cubic_meter.category:
            volume += Uom.compute_qty(uom, quantity, cubic_meter, round=False)
    return volume


