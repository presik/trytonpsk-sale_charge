# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, fields
from trytond.pool import PoolMeta


class AgentNeighbourhood(ModelSQL):
    "Agent - Zones"
    __name__ = 'sale.sale_charge.agent_neighbourhood'
    _table = 'sale-sale_charge-agent_neighbourhood_rel'
    agent = fields.Many2One('commission.agent', 'Agent', ondelete='CASCADE',
        required=True)
    neighbourhood = fields.Many2One('country.city.neighbourhood',
        'Neighbourhood', ondelete='RESTRICT', required=True)


class Agent(metaclass=PoolMeta):
    __name__ = 'commission.agent'
    # active = fields.Boolean('Active')
    neighbourhood = fields.Many2Many('sale.sale_charge.agent_neighbourhood',
        'agent', 'neighbourhood', 'Neighbourhood')
    # user = fields.Many2One('res.user', 'user')

    @staticmethod
    def default_active():
        return True
