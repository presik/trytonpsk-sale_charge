# This file is part of Tryton.  The COPYRIGHT file at the top level of this
# repository contains the full copyright notices and license terms.
from trytond.pool import Pool

from . import charge, configuration, party, sale, zone


def register():
    Pool.register(
        charge.Charge,
        charge.ChargeLine,
        charge.ChargeSaleReturn,
        charge.ChargeShipment,
        charge.ChargeSaleReturnKind,
        charge.CreateChargeStart,
        charge.PaymentSalesStart,
        charge.GroupPaymentApplied,
        charge.ChargePaymentLine,
        charge.PaymentListSale,
        party.Address,
        zone.City,
        zone.CityAddittionalSubdivision,
        sale.Sale,
        configuration.Configuration,
        module='sale_charge', type_='model')
    Pool.register(
        charge.ChargeReport,
        charge.ChargeReportSummary,
        module='sale_charge', type_='report')
    Pool.register(
        charge.CreateCharge,
        charge.PaymentSales,
        module='sale_charge', type_='wizard')
