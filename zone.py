# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import DeactivableMixin, ModelSQL, ModelView, fields
from trytond.pool import PoolMeta
from trytond.pyson import Bool, Eval


class City(metaclass=PoolMeta):
    __name__ = 'country.city'
    additional_subdivisions = fields.One2Many('city.additional_subdivision',
        'city', 'Additional Subdivisions')


class CityAddittionalSubdivision(DeactivableMixin, ModelSQL, ModelView):
    "City Addittional Subdivision"
    __name__ = "city.additional_subdivision"

    name = fields.Char('Name', required=True, translate=True,
        help="The main identifier of the subdivision.")
    city = fields.Many2One('country.city', 'City', states={
        'invisible': Bool(Eval('parent')),
        'required': ~Eval('parent'),
    })
    code = fields.Char('Code', required=True)
    type = fields.Selection([
        (None, ""),
        ('borough', 'Borough'),
        ('zone', 'zone'),
        ], 'Type', required=True, depends=['parent', 'city'])
    parent = fields.Many2One('city.additional_subdivision', 'Parent',
        states={'invisible': Eval('type') != 'borough'},
        help="Add subdivision below the parent.")
    childs = fields.One2Many('city.additional_subdivision', 'parent', 'Boroughs',
        states={'invisible': Eval('type') != 'zone'})

    @classmethod
    def __setup__(cls):
        super(CityAddittionalSubdivision, cls).__setup__()
        cls._order.insert(0, ('code', 'ASC'))

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
            ('name', *tuple(clause[1:])),
            ('code', *tuple(clause[1:])),
            ]

    def default_type():
        return 'zone'

    @fields.depends('parent', 'type')
    def on_change_parent(self):
        result = 'borough' if self.parent else 'zone'
        self.type = result
