# This file is part of Tryton.  The COPYRIGHT file at the top level
# of this repository contains the full copyright notices and license terms.
from decimal import Decimal
from functools import partial
from itertools import groupby

from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.modules.company import CompanyReport
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.tools import sortable_values
from trytond.transaction import Transaction
from trytond.wizard import Button, StateTransition, StateView, Wizard

from .common import parcel_volume, parcel_weight
from .exceptions import PaymentTermMissingError

STATES = {
    'readonly': (Eval('state') != 'draft'),
}


class Charge(Workflow, ModelSQL, ModelView):
    "Sale Charge"
    __name__ = 'sale.charge'
    party = fields.Many2One('party.party', 'Party', states=STATES)
    price_list = fields.Many2One('product.price_list', 'Price List',
        domain=[('company', '=', Eval('company'))], states=STATES)
    location = fields.Many2One('stock.location', 'Location', states=STATES,
        domain=[('type', '=', 'warehouse')])
    date = fields.Date('Date', required=True, states=STATES)
    lines = fields.One2Many('sale.charge.line', 'charge', 'Lines',
        states=STATES)
    company = fields.Many2One('company.company', 'Company', required=True,
        states=STATES)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirm'),
        ('done', 'Done'),
        ('cancel', 'Canceled'),
        ], 'State', readonly=True)

    sale_rebate = fields.Numeric('Rebate', digits=(16, 2), states=STATES,
        help='Rebate in percentage')
    boroughs = fields.Function(fields.One2Many(
        'city.additional_subdivision',
        None, 'Boroughs', states=STATES), 'get_boroughs')
    sales = fields.One2Many('sale.sale', 'charge', 'Sales', add_remove=[], states=STATES,
        filter=[('state', 'not in', ['draft', 'cancelled', 'done'])])
    shipments = fields.Many2Many('sale_charge.shipment', 'charge', 'shipment', 'Shipments',
        filter=[('state', '=', 'packed')])
    return_sales = fields.One2Many('sale.charge.sale_return', 'charge',
        'Return Sales', states={'readonly': Eval('state').in_(['done', 'cancel'])})
    total_charge = fields.Function(fields.Numeric('Total Charge',
        digits=(16, 2)), 'get_total_amount')
    vehicle_plate = fields.Char('Vehicle Plate', states=STATES)
    main_driver = fields.Char('Main Driver', states=STATES)
    aux_driver = fields.Char('Auxiliar Driver', states=STATES)
    second_aux = fields.Char('Second Auxiliar', states=STATES)
    orders_quantity = fields.Function(fields.Integer('Orders Qty'),
        'get_orders_quantity')
    dispatched_by = fields.Char('Dispatched By', states=STATES)
    amount_delivered_rate = fields.Function(fields.Numeric(
        'Amount Delivered Rate', digits=(16, 2)), 'get_amount_delivered_rate')
    freight_value = fields.Numeric('Freight Value', digits=(16, 2),
        states=STATES)
    statements = fields.Function(fields.Many2Many('account.statement',
        None, None, 'Statements'), 'get_statements')
    payments_applied = fields.One2Many("sale.charge.group_payment", "charge", "Payments")
    packages_quantity = fields.Function(fields.Integer('Packages Quantity'),
        'get_packages_quantity')
    comment = fields.Text('Comment')
    number = fields.Char('Number', states={'readonly': True})
    weight = fields.Function(fields.Float('Weight (kg)', states={'readonly': True}), '_get_weight')
    volume = fields.Function(fields.Float('Volume (m³)', states={'readonly': True}), '_get_volume')

    @classmethod
    def __setup__(cls):
        super(Charge, cls).__setup__()
        cls._order.insert(0, ('date', 'DESC'))
        cls._transitions |= set((
            ('draft', 'confirm'),
            ('draft', 'cancel'),
            ('confirm', 'done'),
        ))
        cls._buttons.update({
            'cancel': {
                'invisible': Eval('state') != 'draft',
            },
            'draft': {
                'invisible': Eval('state') != 'confirm',
            },
            'confirm': {
                'invisible': Eval('state') != 'draft',
            },
        })

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, charges):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, charges):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirm')
    def confirm(cls, charges):
        for charge in charges:
            charge.set_number()

    def set_number(self):
        configuration = Pool().get('sale.configuration')(1)
        if configuration.charge_sequence and not self.number:
            self.number = configuration.charge_sequence.get()
            self.save()
        else:
            raise UserError(
                gettext('sale_charge.msg_charge_sequence_missing'))

    def _group_parcel_key(self, lines, line):
        """
        The key to group lines by parcel
        """
        return ()

    def _get_weight(self, name):
        weights = []
        lines = []
        for sale in self.sales:
            lines.extend(sale.lines)
        keyfunc = partial(self._group_parcel_key, lines)
        lines = sorted(lines, key=sortable_values(keyfunc))

        for _, parcel in groupby(lines, key=keyfunc):
            weights.append(parcel_weight(parcel, 'unit'))
        return sum(weights)

    def _get_volume(self, name):
        volumes = []
        lines = []
        for sale in self.sales:
            lines.extend(sale.lines)
        keyfunc = partial(self._group_parcel_key, lines)
        lines = sorted(lines, key=sortable_values(keyfunc))

        for _, parcel in groupby(lines, key=keyfunc):
            volumes.append(parcel_volume(parcel, 'unit'))
        return sum(volumes)

    @classmethod
    def _create_sale(cls, charge):
        Party = Pool().get('party.party')
        PaymentTerm = Pool().get('account.invoice.payment_term')
        SaleLine = Pool().get('sale.line')
        Sale = Pool().get('sale.sale')
        Product = Pool().get('product.product')

        payment = PaymentTerm.search([])
        if not payment:
            raise PaymentTermMissingError(
                gettext('sale_charge.msg_payment_term_missing'))
        price_list = None if not charge.price_list else charge.price_list.id
        sale, = Sale.create([{
            'company': charge.company.id,
            'payment_term': payment[0].id,
            'party': charge.party.id,
            'price_list': price_list,
            'sale_date': charge.date,
            'warehouse': charge.location.id,
            'state': 'draft',
            'invoice_address': Party.address_get(charge.party, type='invoice'),
            'shipment_address': Party.address_get(charge.party, type='delivery'),
            'description': ('Planilla'),
        }])

        for charge_line in charge.lines:
            if charge_line.settle <= 0:
                continue

            line = SaleLine()
            line.unit = charge_line.product.sale_uom
            line.sale = sale.id
            with Transaction().set_context(line._get_context_sale_price()):
                unit_price = Product.get_sale_price([charge_line.product],
                        charge_line.settle or 0)[charge_line.product.id]
                unit_price = Decimal(str(round(unit_price, 2)))
            SaleLine.create([{
                'sale': sale.id,
                'type': 'line',
                'unit': charge_line.product.default_uom.id,
                'quantity': charge_line.settle,
                'unit_price': unit_price,
                'product': charge_line.product.id,
                'taxes': [('add', charge_line.product.customer_taxes_used)],
                'description': charge_line.product.name,
            }])

    def get_packages_quantity(self, name=None):
        # res = []
        # for line in self.lines:
        #     res.append(line.request)
        return int(sum(line.request for line in self.lines))

    def get_statements(self, name=None):
        statements = set(p.statement.id for s in self.sales for p in s.payments)
        return list(statements)

    def get_boroughs(self, name=None):
        borougs = set(sale.shipment_address.borough.id for sale in self.sales if sale.shipment_address.borough)
        return list(borougs)

    def get_orders_quantity(self, name=None):
        if self.sales:
            return len(self.sales)
        return 0

    @fields.depends('lines', methods=['update_lines', 'recompute_lines'])
    def on_change_sales(self):
        self.update_lines()
        # computed_lines = self.recompute_lines()
        # ChargeLine = Pool().get('sale.charge.line')
        # lines_keys = []
        # lines = list(self.lines or [])
        # for line in (self.lines or []):
        #     key = str(line.product.id) + ' ' + str(line.uom.id)
        #     if (key not in computed_lines) or (key in lines_keys):
        #         lines.remove(line)
        #         continue
        #     lines_keys.append(key)
        #     line.amount = computed_lines[key]['amount']
        #     line.request = computed_lines[key]['request']

        # for key, v in computed_lines.items():
        #     if key not in lines_keys:
        #         value = ChargeLine.default_get(
        #             list(ChargeLine._fields.keys()), with_rec_name=False)
        #         line = ChargeLine(**value)
        #         line.request = v['request']
        #         line.product = v['product']
        #         line.uom = v['uom']
        #         lines.append(line)
        # self.lines = lines

    @fields.depends('lines', methods=['recompute_lines'])
    def on_change_shipments(self):
        self.update_lines()

    def update_lines(self):
        computed_lines = self.recompute_lines()
        ChargeLine = Pool().get('sale.charge.line')
        lines_keys = []
        lines = list(self.lines or [])
        for line in (self.lines or []):
            key = str(line.product.id) + ' ' + str(line.uom.id)
            if (key not in computed_lines) or (key in lines_keys):
                lines.remove(line)
                continue
            lines_keys.append(key)
            line.amount = computed_lines[key]['amount']
            line.request = computed_lines[key]['request']

        for key, v in computed_lines.items():
            if key not in lines_keys:
                value = ChargeLine.default_get(
                    list(ChargeLine._fields.keys()), with_rec_name=False)
                line = ChargeLine(**value)
                line.request = v['request']
                line.product = v['product']
                line.uom = v['uom']
                lines.append(line)
        self.lines = lines

    @fields.depends('sales', 'shipments')
    def recompute_lines(self):
        sale_lines = [ln for s in self.sales for ln in s.lines]
        computed_lines = {}
        for line in sale_lines:
            prd_id = line.product.id
            unit_id = line.unit.id
            amount_ = Decimal(line.quantity) * Decimal(str(int(line.unit_price_w_tax)))
            key = str(prd_id) + ' ' + str(unit_id)
            if key not in computed_lines:
                computed_lines[key] = {
                    'product': prd_id,
                    'uom': unit_id,
                    'request': 0,
                    'amount': 0,
                    'charge': None,
                }
            computed_lines[key]['request'] += line.quantity
            computed_lines[key]['amount'] += amount_

        shipment_lines = [ln for s in self.shipments for ln in s.outgoing_moves]
        for line in shipment_lines:
            prd_id = line.product.id
            unit_id = line.unit.id
            amount_ = Decimal(line.quantity) * Decimal(str(int(line.origin.unit_price_w_tax)))
            key = str(prd_id) + ' ' + str(unit_id)
            if key not in computed_lines:
                computed_lines[key] = {
                    'product': prd_id,
                    'uom': unit_id,
                    'request': 0,
                    'amount': 0,
                    'charge': None,
                }
            computed_lines[key]['request'] += line.quantity
            computed_lines[key]['amount'] += amount_
        return computed_lines

    def get_amount_delivered_rate(self, name=None):
        total_paid_sales = 0
        total_sales = 0
        rate = 0
        for sale in self.sales:
            total_paid_sales += sale.paid_amount
            total_sales = sale.total_amount
        for sale_ in self.return_sales:
            if sale_.sale_return:
                total_paid_sales += sale_.sale_return.total_amount
        if total_sales:
            rate = (total_paid_sales / total_sales) * 100
        return Decimal(str(round(rate, 2)))

    def get_total_amount(self, name=None):
        return sum([sale.total_amount for sale in self.sales])


class ChargeLine(ModelSQL, ModelView):
    "Sale Charge Line"
    __name__ = 'sale.charge.line'
    product = fields.Many2One('product.product', 'Product', required=True,
            domain=[('salable', '=', 'True')])
    uom = fields.Many2One('product.uom', 'UOM')
    unit_digits = fields.Function(fields.Integer('Unit Digits'),
            'get_unit_digits')
    request = fields.Float('Request', digits=(16, Eval('unit_digits', 0)),
            depends=['unit_digits'])
    sell_off = fields.Float('Sell Off', digits=(16, 0))
    product_returned = fields.Float('Returned', digits=(16, 0))
    settle = fields.Function(fields.Float('Settle', digits=(16, 0),
            readonly=True), 'get_settle')
    charge = fields.Many2One('sale.charge', 'Charge',
            ondelete='CASCADE')
    sale_lines = fields.Function(fields.One2Many('sale.line', 'product', 'Sale Lines'),
        'get_sale_lines')
    shipment_lines = fields.Function(fields.One2Many('stock.move', 'product', 'Shipment Lines'),
        'get_shipment_lines')
    amount = fields.Numeric('Amount', digits=(16, 2))
    boroughs = fields.Function(fields.Char('Neighbourhood'), 'get_boroughs')

    @classmethod
    def __setup__(cls):
        super(ChargeLine, cls).__setup__()
        cls._order.insert(0, ('product', 'ASC'))

    @staticmethod
    def default_unit_digits():
        return 2

    @staticmethod
    def default_request():
        return 0

    @staticmethod
    def default_sell_off():
        return 0

    @staticmethod
    def default_product_returned():
        return 0

    @fields.depends('product', 'uom', 'unit_digits')
    def on_change_product(self):
        if self.product:
            self.uom = self.product.default_uom.id
            self.unit_digits = self.product.default_uom.digits
            self.unit_digits = 2

    def get_uom(self, name):
        return self.product.default_uom.id

    def get_unit_digits(self, name):
        return self.product.default_uom.digits

    def get_settle(self, name):
        sell_off = self.sell_off or 0
        request = self.request or 0
        returned = self.product_returned or 0
        return ((sell_off + request) - returned)

    def get_boroughs(self, name=None):
        if self.sale_lines:
            boroughs = set(ln.sale.shipment_address.borough.name for ln in self.sale_lines if ln.sale.shipment_address.borough)
            return ' - '.join(boroughs)

    @classmethod
    def get_sale_lines(cls, records, name):
        result = {}
        for record in records:
            lines = []
            if record.product and record.charge.sales:
                lines = [ln.id for s in record.charge.sales for ln in s.lines if ln.product == record.product]
            result[record.id] = lines
        return result

    @classmethod
    def get_shipment_lines(cls, records, name):
        result = {}
        shipments = set(records[0].charge.shipments)
        for record in records:
            lines = []
            if record.product and shipments:
                lines = [ln.id for s in shipments for ln in s.outgoing_moves if ln.product == record.product]
            result[record.id] = lines
        return result


class ChargeSaleReturn(ModelSQL, ModelView):
    "Charge Sale Return"
    __name__ = 'sale.charge.sale_return'
    charge = fields.Many2One('sale.charge', 'Charge', required=True)
    sale = fields.Many2One('sale.sale', 'Sale', required=True)
    kind = fields.Many2One('sale.charge.sale_return.kind', 'Kind', required=True)
    sale_return = fields.Many2One('sale.sale', 'Sale Return')
    # return_amount = fields.Function(fields.Numeric('Amount', digits=(16, 2)),
    #     'on_change_with_return_amount')
    notes = fields.Text('Notes')

    @classmethod
    def __setup__(cls):
        super(ChargeSaleReturn, cls).__setup__()
        cls._buttons.update({
            'manage_sale_return': {
                'invisible': Eval('sale_return'),
            },
        })

    @classmethod
    @ModelView.button
    def manage_sale_return(cls, records):
        for rec in records:
            if not rec.sale:
                return
            rec.manage_return()

    def manage_return(self):
        Sale = Pool().get('sale.sale')
        return_sale, = Sale.copy([self.sale])
        return_sale.origin = self.sale
        return_sale.comment = self.notes
        for line in return_sale.lines:
            if line.type == 'line':
                line.quantity *= -1
            return_sale.lines = return_sale.lines  # Force saving
        Sale.save([return_sale])
        self.sale_return = return_sale.id
        self.save()


class ChargeShipment(ModelSQL, ModelView):
    "Charge Shipment"

    __name__ = "sale_charge.shipment"

    shipment = fields.Many2One('stock.shipment.out', 'Shipment')
    charge = fields.Many2One('sale.charge', 'Charge')


class GroupPaymentApplied(ModelSQL, ModelView):
    "Sale Charge Group Payment"

    __name__ = "sale.charge.group_payment"

    charge = fields.Many2One('sale.charge', 'Charge')
    statement = fields.Many2One('account.statement', 'Statement')
    amount = fields.Function(fields.Numeric('Amount', digits=(16, 4)), 'get_payment')
    lines = fields.One2Many('account.statement.line-charge.group_payment', 'group_payment', 'Statement Line')

    def get_payment(self, name=None):
        return sum(ln.line.amount for ln in self.lines)


class ChargePaymentLine(ModelSQL, ModelView):
    "Charge Payment Line"

    __name__ = "account.statement.line-charge.group_payment"

    group_payment = fields.Many2One("sale.charge.group_payment", 'Group')
    line = fields.Many2One("account.statement.line", 'Line')
    sale = fields.Function(fields.Many2One('sale.sale', 'Sale'), 'get_statement_line_info')
    amount = fields.Function(fields.Numeric('Amount', digits=(16, 4)), 'get_statement_line_info')

    @classmethod
    def get_statement_line_info(cls, records, names):
        result = {n: {} for n in names}
        for record in records:
            for name in names:
                if name == 'sale':
                    result[name][record.id] = getattr(record.line, name).id
                else:
                    result[name][record.id] = getattr(record.line, name)
        return result


class ChargeSaleReturnKind(ModelSQL, ModelView):
    "Charge Sale Return Kind"
    __name__ = 'sale.charge.sale_return.kind'
    name = fields.Char('Name', required=True)


class ChargeReport(CompanyReport):
    __name__ = 'sale.charge'


class ChargeReportSummary(CompanyReport):
    __name__ = 'sale.charge_summary'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        products = {}
        date_list = []
        for charge in records:
            for line in charge.lines:
                if line.product.id in products:
                    products[line.product.id]['quantity'] += line.settle
                else:
                    products[line.product.id] = {
                            'code': line.product.code,
                            'product': line.product.name,
                            'uom': line.product.default_uom.rec_name,
                            'quantity': line.settle,
                            }
            date_list.append(charge.date)
        report_context['products'] = products.values()
        report_context['date_start'] = min(date_list)
        report_context['date_end'] = max(date_list)

        return report_context


class CreateChargeStart(ModelView):
    "Create Charge Start"
    __name__ = 'sale_charge.create_charge.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    date = fields.Date('Date', required=True)
    zones = fields.Many2Many('city.additional_subdivision', None, None,
        'Zones', domain=[('type', '=', 'zone')])
    boroughs = fields.Many2Many('city.additional_subdivision', None, None,
        'Boroughs', domain=[('type', '=', 'borough')])
    sales = fields.Many2Many('sale.sale', None, None, 'Sales', domain=[
        ('state', '=', 'processing'),
        ('charge', '=', None)])
    shipments = fields.Many2Many('sale_charge.shipment', None, None, 'Shipments')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()


class CreateCharge(Wizard):
    "Create Charge"
    __name__ = 'sale_charge.create_charge'
    start = StateView(
        'sale_charge.create_charge.start',
        'sale_charge.create_charge_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        Charge = pool.get('sale.charge')
        ChargeLine = pool.get('sale.charge.line')

        dom = [
            ('state', 'in', ['processing']),
            ('shipment_date', '=', self.start.date),
            ('charge', '=', None),
            ('company', '=', self.start.company.id)]
        if self.start.boroughs:
            borough_ids = [neg.id for neg in self.start.boroughs]
            dom.append(('shipment_address.borough', 'in', borough_ids))

        if self.start.zones:
            zone_ids = [neg.id for neg in self.start.zones]
            dom.append(('shipment_address.zone', 'in', zone_ids))

        sales = Sale.search(dom, order=[
            ('shipment_address.borough', 'ASC'), ('party.id', 'ASC')])

        sales_selected = list(self.start.sales)
        if sales_selected:
            sales.extend(sales_selected)
            sales = list(set(sales))

        create_ = {
            'party': None,
            'price_list': None,
            'date': self.start.date,
            'company': self.start.company.id,
            'state': 'draft',
        }
        charge, = Charge.create([create_])
        create_lines = {}
        for sale in sales:
            if sale.total_amount <= 0:
                continue
            for line in sale.lines:
                prd_id = line.product.id
                unit_id = line.unit.id
                amount_ = line.quantity * int(line.unit_price_w_tax)
                key = str(prd_id) + ' ' + str(unit_id)
                if key not in create_lines:
                    create_lines[key] = {
                        'product': prd_id,
                        'uom': unit_id,
                        'request': 0,
                        'amount': 0,
                        'charge': None,
                    }
                create_lines[key]['charge'] = charge.id
                create_lines[key]['request'] += line.quantity
                create_lines[key]['amount'] += round(amount_, 2)

        for shipment in self.start.shipments:
            for line in shipment.outgoing_moves:
                prd_id = line.product.id
                unit_id = line.unit.id
                amount_ = line.quantity * int(line.origin.unit_price_w_tax)
                key = str(prd_id) + ' ' + str(unit_id)
                if key not in create_lines:
                    create_lines[key] = {
                        'product': prd_id,
                        'uom': unit_id,
                        'request': 0,
                        'amount': 0,
                        'charge': None,
                    }
                create_lines[key]['charge'] = charge.id
                create_lines[key]['request'] += line.quantity
                create_lines[key]['amount'] += round(amount_, 2)

        ChargeShipment = Pool().get('sale_charge.shipment')
        ChargeShipment.create([{'charge': charge.id, 'shipment': shipment.id} for shipment in self.start.shipments])

        Sale.write(sales, {'charge': charge.id})
        ChargeLine.create(create_lines.values())
        return 'end'


class PaymentSalesStart(ModelView):
    "Payment Sales Start"
    __name__ = 'sale_charge.payment_sales.start'
    statement = fields.Many2One('account.statement', 'Statement',
        required=True, domain=[('state', '=', 'draft')])
    sales = fields.One2Many('payment.list.sale', 'line', 'Sales')
    total_amount = fields.Function(fields.Numeric('Total Amount',
        digits=(16, 2), depends=['sales']), 'on_change_with_total_amount')

    id = fields.Integer('Id', readonly=True)
    company = fields.Many2One('company.company', 'Company', readonly=True)

    @staticmethod
    def default_id():
        return 1

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('sales')
    def on_change_with_total_amount(self, name=None):
        res = 0
        if self.sales:
            res = sum([s.amount_to_pay for s in self.sales])
        return res


class PaymentListSale(ModelView):
    "Payment List Sale"

    __name__ = "payment.list.sale"

    line = fields.Many2One('sale_charge.payment_sales.start', 'Line', readonly=True)
    sale = fields.Many2One('sale.sale', 'Sale', domain=[('charge', '=', Eval('charge'))])
    charge = fields.Many2One('sale.charge', 'Charge', readonly=True)
    description = fields.Char('Description')
    amount_pending = fields.Numeric('Amount Pending', readonly=True, digits=(16, 2))
    amount_to_pay = fields.Numeric('Amount To Pay', digits=(16, 2))

    @staticmethod
    def default_charge():
        return Transaction().context.get('active_id')

    @staticmethod
    def default_line():
        return 1

    @fields.depends('sale', 'amount_pending', 'amount_to_pay')
    def on_change_sale(self):
        if self.sale:
            amount = self.sale.residual_amount
            self.amount_pending = amount
            self.amount_to_pay = amount


class PaymentSales(Wizard):
    "Payment Sales"
    __name__ = 'sale_charge.payment_sales'
    start = StateView(
        'sale_charge.payment_sales.start',
        'sale_charge.sale_charge_payment_sales_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'pay_', 'tryton-ok', default=True),
        ])
    pay_ = StateTransition()

    def default_start(self, fields):
        sales = []
        charge_id = Transaction().context.get('active_id')
        charge = Charge(charge_id)
        for sale in charge.sales:
            amount = sale.residual_amount
            if amount == 0:
                continue
            value = {
                'id': sale.id,
                'sale': sale.id,
                'amount_pending': amount,
                'amount_to_pay': amount,
                'charge': charge_id,
                'description': None,
            }
            sales.append(value)

        for credit in charge.return_sales:
            credit = credit.sale_return
            amount = credit.residual_amount
            if amount == 0:
                continue
            credit_id = credit.id
            value = {
                'id': credit_id,
                'sale': credit_id,
                'amount_pending': amount,
                'amount_to_pay': amount,
                'charge': charge_id,
                'description': None,
            }
            sales.append(value)
        sales_shipments = set(m.sale for s in charge.shipments
            for m in s.outgoing_moves if m.sale)
        for sale in sales_shipments:
            amount = sale.residual_amount
            if amount == 0:
                continue
            value = {
                'id': sale.id,
                'sale': sale.id,
                'amount_pending': amount,
                'amount_to_pay': amount,
                'charge': charge_id,
                'description': None,
            }
            sales.append(value)

        return {'sales': sales}

    def transition_pay_(self):

        pool = Pool()
        Date = pool.get('ir.date')
        Sale = pool.get('sale.sale')
        StatementLine = pool.get('account.statement.line')
        PaymentGroup = pool.get('sale.charge.group_payment')
        today = Date.today()
        statement_id = self.start.statement.id
        charge_id = Transaction().context.get('active_id')
        lines_statement = []
        for s in self.start.sales:
            sale = s.sale
            if not sale.number or sale.residual_amount == 0:
                continue
            Sale.post_invoices(sale)
            Sale.do_stock_moves([sale])
            account_id = sale.party.account_receivable.id

            invoice_number = ''
            if sale.invoice_number:
                invoice_number = sale.invoice_number
            elif sale.invoice and sale.invoice.number:
                invoice_number = sale.invoice.number

            to_create = {
                'sale': sale.id,
                'date': today,
                'statement': statement_id,
                'amount': s.amount_to_pay,
                'party': sale.party.id,
                'account': account_id,
                'description': invoice_number,
            }
            line, = StatementLine.create([to_create])
            turn = line.statement.turn
            sale_device = line.statement.sale_device
            value = {
                'turn': turn,
                'sale_device': sale_device,
            }
            if 'order_status' in Sale._fields:
                value['order_status'] = 'delivered'
            if s.description:
                description = s.description
                if sale.description:
                    description = sale.description + ' ' + description

                value['description'] = description
            Sale.write([sale], value)
            if sale.invoice:
                invoice = sale.invoice
                invoice.turn = turn
                invoice.save()
            Sale.do_reconcile([sale])
            lines_statement.append({'line': line})

        payment_applied = {
            'statement': statement_id,
            # 'amount': self.start.total_amount,
            'charge': charge_id,
            'lines': [('create', lines_statement)],
            }
        PaymentGroup.create([payment_applied])
        return 'end'
