# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval


class Address(metaclass=PoolMeta):
    __name__ = 'party.address'

    zone = fields.Many2One('city.additional_subdivision', 'Zone',
        domain=[
            ('type', '=', 'zone'),
            ('city', '=', Eval('city_co'))])

    borough = fields.Many2One('city.additional_subdivision', 'Borough',
        domain=[
            ('type', '=', 'borough'),
            ('parent', '=', Eval('zone'))])


class PartyDunningReport(metaclass=PoolMeta):
    __name__ = 'invoice_report.party_dunning'

    @classmethod
    def get_invoices(cls, party):
        Invoice = Pool().get('account.invoice')
        invoices = Invoice.search([
            ['OR',
                ('agent.party', '=', party.id),
                ('party', '=', party.id),
            ], [
                ('state', 'in', ['posted', 'validated']),
                ('type', '=', 'out'),
            ]], order=[
                ('reference', 'ASC'),
                ('invoice_date', 'ASC'),
            ])
        return invoices
